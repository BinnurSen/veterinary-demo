package com.binnur.veterinary.repository;

import com.binnur.veterinary.model.Animal;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AnimalRepository  extends MongoRepository<Animal,String> {

    Animal findByAnimalId(String animalId);

    List<Animal> findByNameContains(String name);

    List<Animal> findByUserNameContains(String name);
}
