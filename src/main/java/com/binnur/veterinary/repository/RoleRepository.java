package com.binnur.veterinary.repository;

import com.binnur.veterinary.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role,String> {

    Role findByRoleId(String roleId);

    Role findByName(String name);
}
