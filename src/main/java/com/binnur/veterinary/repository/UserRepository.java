package com.binnur.veterinary.repository;

import com.binnur.veterinary.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User,String> {
    User findByUserId(String userId);

    List<User> findByNameEquals(String name);

    List<User> findByNameContains(String name);

    User findByEmail (String email);


}
