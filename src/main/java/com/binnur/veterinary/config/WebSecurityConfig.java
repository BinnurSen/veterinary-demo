package com.binnur.veterinary.config;

import com.binnur.veterinary.util.TokenOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    TokenOperations tokenOperations;

    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().and().csrf().disable().authorizeRequests()
                .antMatchers("/test/**").hasRole("ADMIN")
                .antMatchers("/userService/**").hasRole("USER")
                .antMatchers("/animalService/**").hasRole("USER")
                .anyRequest().authenticated()
                .and()
                .addFilterAfter(new JwtFilter(tokenOperations), BasicAuthenticationFilter.class).
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(WebSecurity web){
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
        web.ignoring().antMatchers(HttpMethod.POST,"/userService/addUser");
        web.ignoring().antMatchers(HttpMethod.GET,"/");
        web.ignoring().antMatchers(HttpMethod.POST,"/login");
        web.ignoring().antMatchers(HttpMethod.GET,"/home");
        web.ignoring().antMatchers(HttpMethod.GET,"/newUser");
        web.ignoring().antMatchers(HttpMethod.GET,"/deleteUser");
        web.ignoring().antMatchers(HttpMethod.GET,"/searchUser");
        web.ignoring().antMatchers(HttpMethod.GET,"/searchAnimal");
        web.ignoring().antMatchers(HttpMethod.GET,"/updateUser");
        web.ignoring().antMatchers(HttpMethod.POST,"/save");
        web.ignoring().antMatchers(HttpMethod.GET,"/allAnimals");
        web.ignoring().antMatchers(HttpMethod.GET,"/newAnimal");
        web.ignoring().antMatchers(HttpMethod.POST,"/saveAnimal");
        web.ignoring().antMatchers(HttpMethod.GET,"/updateAnimal");
        web.ignoring().antMatchers(HttpMethod.GET,"/deleteAnimal");
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("DELETE");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }


    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new Http403ForbiddenEntryPoint();
    }

}

