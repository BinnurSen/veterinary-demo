package com.binnur.veterinary.config;

import com.binnur.veterinary.model.User;
import com.binnur.veterinary.util.TokenOperations;
import com.binnur.veterinary.util.enums.JwtFilterEnum;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class JwtFilter extends GenericFilterBean {

    private final TokenOperations tokenOperations;

    public JwtFilter(TokenOperations tokenProvider) {
        this.tokenOperations = tokenProvider;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
//            HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
            Map<String, String> tokenVerifyResponse = checkTokenVerify(httpServletRequest);
            String status = tokenVerifyResponse.get(JwtFilterEnum.STATUS.getField());
            if (JwtFilterEnum.TRUE.getField().equals(status)) {
                Authentication authentication = getAuthentication(httpServletRequest);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                SecurityContextHolder.clearContext();
                throw new ServletException(JwtFilterEnum.BAD_REQUEST.getField());

            }
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
//            Sentry.capture(e);
            try {
                throw e;
            } catch (NoSuchAlgorithmException e1) {
                e1.printStackTrace();
            } catch (InvalidKeySpecException e1) {
                e1.printStackTrace();
            }
        }
    }

    private Map<String, String> checkTokenVerify(HttpServletRequest request) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        Map<String, String> verifyTokenResponse = new HashMap<>();
        String bearerToken = request.getHeader(JwtFilterEnum.AUTHORIZATION.getField());
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(JwtFilterEnum.BEARER.getField())) {
            String token = bearerToken.substring(7);
            boolean isVerify = tokenOperations.verifyToken(request);
            if (isVerify) {
                verifyTokenResponse.put(JwtFilterEnum.STATUS.getField(), JwtFilterEnum.TRUE.getField());
                verifyTokenResponse.put(JwtFilterEnum.JWT.getField(), token);
                return verifyTokenResponse;
            } else {
                verifyTokenResponse.put(JwtFilterEnum.STATUS.getField(), JwtFilterEnum.FALSE.getField());
                return verifyTokenResponse;
            }
        } else {
            verifyTokenResponse.put(JwtFilterEnum.STATUS.getField(), JwtFilterEnum.FALSE.getField());
            return verifyTokenResponse;
        }
    }

    public Authentication getAuthentication(HttpServletRequest request) throws ServletException {
        User user = tokenOperations.decodeToken(request);
        GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole());
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getPhone(), user.getPassword(), Arrays.asList(authority));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

}
