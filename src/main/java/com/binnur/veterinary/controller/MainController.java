package com.binnur.veterinary.controller;

import com.binnur.veterinary.model.User;
import com.binnur.veterinary.repository.AnimalRepository;
import com.binnur.veterinary.repository.UserRepository;
import com.binnur.veterinary.util.GenericResponse;
import com.binnur.veterinary.util.ObjectMapperUtil;
import com.binnur.veterinary.util.RestTemplateUtil;
import com.binnur.veterinary.util.TokenOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MainController {

    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    private static final String TOKEN = "token";

    private static final String USER_NAME = "username";

    private static final String USER_ID = "userId";

    private GenericResponse response;

    @Autowired
    TokenOperations tokenOperations;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AnimalRepository animalRepository;

    @GetMapping("/home")
    public String init(HttpServletRequest request){
        request.setAttribute("userData", userRepository.findAll());
        request.setAttribute("mode", "USER_VIEW");
        return "index";
    }
    @GetMapping("/searchUser")
    public String searchUser(@RequestParam String search, HttpServletRequest request){
        String a = search;
        String[] splitSearch = a.split(" ");
        if(splitSearch.length>1){
            List<User> byNameEquals = userRepository.findByNameContains(splitSearch[0].toLowerCase());
            if(byNameEquals.size()>0){
                request.setAttribute("userData", byNameEquals);
                request.setAttribute("mode", "USER_VIEW");
            }
            List<User> byNameEquals1 = userRepository.findByNameContains(splitSearch[1].toLowerCase());
            if(byNameEquals1.size()>0){
                request.setAttribute("userData", byNameEquals1);
                request.setAttribute("mode", "USER_VIEW");
            }

        }else if(splitSearch.length == 1){
            List<User> byNameEquals = userRepository.findByNameContains(splitSearch[0].toLowerCase());
            if(byNameEquals.size()>0){
                request.setAttribute("userData", byNameEquals);
                request.setAttribute("mode", "USER_VIEW");
            }
        }



        return "index";
    }


    @GetMapping("/updateUser")
    public String updateUser(@RequestParam String userId, HttpServletRequest request, Model model){
        User byUserId = userRepository.findByUserId(userId);
        if(byUserId.getLoggedIn() == 0){
            model.addAttribute("loginError", "Error logging in. Please try again.");
            return "login";
        }else if(byUserId.getRole() =="ROLE_ADMIN" && byUserId.getLoggedIn() == 1){
            request.setAttribute("user", userRepository.findByUserId(userId));
            request.setAttribute("mode", "USER_EDIT");
            return "index";
        }
        return "index";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute User user, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        user.setName(user.getName().toLowerCase());
        user.setRole("USER");
        user.setPassword(passwordEncoder().encode(user.getPassword()));

        userRepository.save(user);
            //model.addAttribute("error", "Yetkiniz Yok");
            //return "error";


        request.setAttribute("userData", userRepository.findAll());
        request.setAttribute("mode", "USER_VIEW");
        response.sendRedirect("/home");
        return "index";


    }

    @GetMapping("/newUser")
    public String newUser(HttpServletRequest request){
        request.setAttribute("mode","USER_NEW");
        return "index";
    }
    @GetMapping("/deleteUser")
    public String deleteUser(@RequestParam String userId, HttpServletRequest request, HttpServletResponse response,Model model) throws IOException {

        User byUserId = userRepository.findByUserId(userId);
        if(byUserId.getLoggedIn() == 0){
            model.addAttribute("loginError", "Error logging in. Please try again.");
            return "login";
        }
        else if(byUserId.getRole() =="ROLE_ADMIN" && byUserId.getLoggedIn() == 1){
            userRepository.delete(byUserId);
            request.setAttribute("userData", userRepository.findAll());
            request.setAttribute("mode", "USER_VİEW");
            return "redirect:home";
        }else{
            model.addAttribute("error", "Yetkiniz Yok.");
            return "error";
        }


    }

    @GetMapping("/getAllUsers")
    public List<User> getAllUsers(){
        List<User> all = userRepository.findAll();
        return all;
    }

    @GetMapping("/")
    public String showLoginForm(){
        return "login";
    }

    @PostMapping("/login")
    public String verifyLogin(@RequestParam String email, @RequestParam String password, HttpSession session, Model model){
        User foundedUser = userRepository.findByEmail(email);
        if(foundedUser != null){
            if (!passwordEncoder().matches(password, foundedUser.getPassword())) {
                model.addAttribute("loginError", "Error logging in. Please try again.");
                return "login";
            }else {
                foundedUser.setLoggedIn(1);
                userRepository.save(foundedUser);
                session.setAttribute("loggedInUser", foundedUser);
                return "redirect:home";
            }
        }else{
            model.addAttribute("loginError", "Error logging in. Please try again.");
            return "login";
        }
    }

    @PostMapping("/logout")
    public String logout(@RequestParam String email, HttpSession session, Model model){
        User foundedUser = userRepository.findByEmail(email);
        if(foundedUser != null){
            foundedUser.setLoggedIn(0);
            userRepository.save(foundedUser);
            model.addAttribute("loginError", "Başarıyla Çıkış yapıldı.");
            return "login";
        }
        return "login";
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }



}
