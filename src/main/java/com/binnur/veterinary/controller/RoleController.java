package com.binnur.veterinary.controller;

import com.binnur.veterinary.model.Role;
import com.binnur.veterinary.repository.RoleRepository;
import com.binnur.veterinary.util.GenericResponse;
import com.binnur.veterinary.util.ResponseUtil;
import com.binnur.veterinary.util.constant.TAServiceEnConstant;
import com.binnur.veterinary.util.constant.TAServiceTrConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/roleService")
public class RoleController {

    @Autowired
    RoleRepository roleRepository;

    private GenericResponse response;


    @PostMapping("/addRole")
    @ResponseBody
    public ResponseEntity<?> addRole(@RequestBody Role role)
    {
        Role byName = roleRepository.findByName(role.getName());
        if(byName == null){
            roleRepository.save(role);
            response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.SUCCESS, TAServiceEnConstant.SUCCESS);

        }else{
            response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.ERROR, TAServiceEnConstant.ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);

    }
}
