package com.binnur.veterinary.controller;

import com.binnur.veterinary.model.Animal;
import com.binnur.veterinary.model.User;
import com.binnur.veterinary.repository.AnimalRepository;
import com.binnur.veterinary.repository.UserRepository;
import com.binnur.veterinary.util.GenericResponse;
import com.binnur.veterinary.util.ResponseUtil;
import com.binnur.veterinary.util.constant.TAServiceEnConstant;
import com.binnur.veterinary.util.constant.TAServiceTrConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class AnimalController {

    @Autowired
    AnimalRepository animalRepository;

    @Autowired
    UserRepository userRepository;


    private GenericResponse response;

    @GetMapping("/searchAnimal")
    public String searchAnimal(@RequestParam String searchAnimal, HttpServletRequest request){
        String a = searchAnimal;
        List<Animal> byNameEquals = animalRepository.findByNameContains(searchAnimal.toLowerCase());
        if(byNameEquals.size() > 0){
            request.setAttribute("animalData", byNameEquals);
            request.setAttribute("mode", "ANIMAL_VIEW");
        }else{
            String[] splitSearch = searchAnimal.split(" ");
            if(splitSearch.length>1){
                List<Animal> byNameEquals1 = animalRepository.findByUserNameContains(splitSearch[0].toLowerCase());
                if(byNameEquals1.size()>0){
                    request.setAttribute("animalData", byNameEquals1);
                    request.setAttribute("mode", "ANIMAL_VIEW");
                }
                List<Animal> byNameEquals2 = animalRepository.findByUserNameContains(splitSearch[1].toLowerCase());
                if(byNameEquals2.size()>0){
                    request.setAttribute("animalData", byNameEquals2);
                    request.setAttribute("mode", "ANIMAL_VIEW");
                }

            }else if(splitSearch.length == 1){
                List<Animal> byNameEquals3 = animalRepository.findByUserNameContains(splitSearch[0].toLowerCase());
                if(byNameEquals3.size()>0){
                    request.setAttribute("animalData", byNameEquals3);
                    request.setAttribute("mode", "ANIMAL_VIEW");
                }
            }
        }



        return "index";
    }

    @GetMapping("/allAnimals")
    public String allAnimals(HttpServletRequest request){
        List<Animal> all = animalRepository.findAll();
        request.setAttribute("animalData", all);
        request.setAttribute("mode", "ANIMAL_VIEW");
        return "index";
    }

    @GetMapping("/updateAnimal")
    public String updateAnimal(@RequestParam String animalId, HttpServletRequest request){
        request.setAttribute("animal", animalRepository.findByAnimalId(animalId));
        request.setAttribute("mode", "ANIMAL_EDIT");
        return "index";
    }

    @PostMapping("/saveAnimal")
    public void saveAnimal(@ModelAttribute  Animal animal, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) throws IOException {
        animal.setName(animal.getName().toLowerCase());
        animal.setUserName(animal.getUserName().toLowerCase());
        animalRepository.save(animal);
        request.setAttribute("animalData", animalRepository.findAll());
        request.setAttribute("mode", "ANIMAL_VIEW");
        response.sendRedirect("/allAnimals");

    }

    @GetMapping("/newAnimal")
    public String newAnimal(HttpServletRequest request){
        request.setAttribute("mode","ANIMAL_NEW");
        return "index";
    }
    @GetMapping("/deleteAnimal")
    public void deleteAnimal(@RequestParam String animalId, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Animal byAnimalId = animalRepository.findByAnimalId(animalId);
        animalRepository.delete(byAnimalId);
        request.setAttribute("animalData", animalRepository.findAll());
        request.setAttribute("mode", "ANIMAL_VİEW");
        response.sendRedirect("/allAnimals");

    }



}
