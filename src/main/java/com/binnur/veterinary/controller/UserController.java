package com.binnur.veterinary.controller;

import com.binnur.veterinary.model.LoginUser;
import com.binnur.veterinary.model.User;
import com.binnur.veterinary.repository.UserRepository;
import com.binnur.veterinary.util.GenericResponse;
import com.binnur.veterinary.util.ResponseUtil;
import com.binnur.veterinary.util.TokenOperations;
import com.binnur.veterinary.util.constant.TAServiceEnConstant;
import com.binnur.veterinary.util.constant.TAServiceTrConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Controller
@RequestMapping("/userService")
public class UserController {

    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    private static final String TOKEN = "token";

    private static final String USER_NAME = "username";

    private static final String USER_ID = "userId";

    @Autowired
    TokenOperations tokenOperations;

    @Autowired
    UserRepository userRepository;

    private GenericResponse response;


    @PostMapping("/addUser")
    @ResponseBody
    public ResponseEntity<?> addUser(@RequestBody User user)
    {
        User byName = userRepository.findByEmail(user.getEmail());
        if(byName == null){
            //user.setPassword(passwordEncoder().encode(user.getPassword()));
            userRepository.save(user);
            response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.SUCCESS, TAServiceEnConstant.SUCCESS);

        }else{
            response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.ERROR, TAServiceEnConstant.ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/deleteUser")
    public ResponseEntity<?> deleteUser(HttpServletRequest request, @QueryParam("userId") String userId) throws ServletException {
        User foundedUser = tokenOperations.decodeToken(request);
        if(foundedUser != null){

            User deletedUser = userRepository.findByUserId(userId);
            if(deletedUser != null)
            {
                userRepository.delete(deletedUser);
                response = ResponseUtil.getInstance().createGenericResponse(true, TAServiceTrConstant.SUCCESS, TAServiceEnConstant.SUCCESS);
            }else {
                response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.ERROR, TAServiceEnConstant.ERROR);
            }
        }else{
            response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.ERROR, TAServiceEnConstant.ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/updateUser")
    @ResponseBody
    public ResponseEntity<?> updateUser(HttpServletRequest request,@PathVariable("userId") String userId, @RequestBody User user) throws ServletException {
        User foundUser = tokenOperations.decodeToken(request);
        if(foundUser !=null){
            User foundedUser = userRepository.findByUserId(userId);

            if(foundedUser != null)
            {
                if (user.getName() != null) {
                    foundedUser.setName(user.getName());
                }

                if (user.getEmail() != null) {
                    foundedUser.setEmail(user.getEmail());
                }
                if (user.getPhone() != null) {
                    foundedUser.setPhone(user.getPhone());
                }
                if (user.getPassword() != null) {
                    foundedUser.setPassword(user.getPassword());
                }
                if (user.getRole() != null) {
                    foundedUser.setRole(user.getRole());
                }
                User savedUser = userRepository.save(foundedUser);
                if(savedUser != null)
                {
                    response = ResponseUtil.getInstance().createGenericResponse(true, TAServiceTrConstant.SUCCESS, TAServiceEnConstant.SUCCESS);
                }else{
                    response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.ERROR, TAServiceEnConstant.ERROR);
                }
            }else
                response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.ERROR, TAServiceEnConstant.ERROR);

        }else
            response = ResponseUtil.getInstance().createGenericResponse(false, TAServiceTrConstant.ERROR, TAServiceEnConstant.ERROR);


        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/getAllUser")
    public List<User> getAllUser(){
        List<User> all = userRepository.findAll();
        return all;
    }



    /*@PostMapping("/login")
    public String verifyLogin(@RequestParam String email, @RequestParam String password, HttpSession session, Model model) throws InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException {
        User foundedUser = userRepository.findByEmail(email);
        if(foundedUser != null){
            if (!passwordEncoder().matches(password, foundedUser.getPassword())) {
                model.addAttribute("loginError", "Error logging in. Please try again.");
                return "login";
            }else {
                Map userMap = new HashMap();
                userMap.put(TOKEN, tokenOperations.getToken(foundedUser));
                foundedUser.setToken(tokenOperations.getToken(foundedUser));
                userMap.put(USER_NAME, foundedUser.getName());
                userMap.put(USER_ID, foundedUser.getUserId());
                userMap.put(ROLE_ADMIN,foundedUser.getRole());
                userRepository.save(foundedUser);
                //return new ResponseEntity<>(userMap, HttpStatus.OK);
                return "redirect:/";
            }
        }else{
            model.addAttribute("loginError", "Error logging in. Please try again.");
            return "login";
        }
    }*/









}
