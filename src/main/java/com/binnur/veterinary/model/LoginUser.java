package com.binnur.veterinary.model;

import lombok.Data;

@Data
public class LoginUser {

    private String email;

    private String password;
}
