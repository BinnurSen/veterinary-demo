package com.binnur.veterinary.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "animal")
@Data
public class Animal {
    @Id
    private  String animalId;

    private String name;

    private String type;

    private String genus;

    private Integer age;

    private String description;

    private String userId;

    private String userName;
}
