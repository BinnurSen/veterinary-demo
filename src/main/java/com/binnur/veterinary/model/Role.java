package com.binnur.veterinary.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "role")
@Data
public class Role {



    @Id
    private String roleId;

    private String name;
}
