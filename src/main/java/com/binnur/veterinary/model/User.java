package com.binnur.veterinary.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "user")
@Data
public class User implements Serializable {

    @Id
    private String userId;

    private  String name;

    private String phone;

    private String email;

    private String Password;

    private String address;

    private String role;

    private String token;

    private Integer loggedIn = 0;
}
