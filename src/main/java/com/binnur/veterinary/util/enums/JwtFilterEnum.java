package com.binnur.veterinary.util.enums;

public enum JwtFilterEnum {

    AUTHORIZATION("Authorization"),
    BEARER("Bearer "),
    BAD_REQUEST("Bad Request"),
    STATUS("status"),
    TRUE("true"),
    FALSE("false"),
    JWT("jwt"),
    INVALID_TOKEN("Invalid Token");

    private String field;

    JwtFilterEnum(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

}
