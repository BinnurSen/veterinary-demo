package com.binnur.veterinary.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.binnur.veterinary.model.User;
import com.binnur.veterinary.repository.UserRepository;
import com.binnur.veterinary.util.enums.JwtFilterEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.time;

@Component
public class TokenOperations {



    private static Logger LOGGER = Logger.getLogger(TokenOperations.class.getName());

    private String privateKeyPath = "C:/Users/feb/Documents/binnur/veterinary/privateKey.key";


    private String publicKeyPath = "C:/Users/feb/Documents/binnur/veterinary/publicKey.pem";

    private static final String INVALID_TOKEN = "Invalid token";

    private static final String TEST = "binnur";

    public static final long EXPIRATION_TIME = 86400000; // 1 day

    private static Base64.Encoder encoder = Base64.getEncoder();

    @Autowired
    UserRepository userRepository;

    public String getToken(User user) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC");
        ECGenParameterSpec genSpec = new ECGenParameterSpec("secp256r1");
        keyGen.initialize(genSpec);
        //KeyPair pair = keyGen.generateKeyPair();
        //createPrivateAndPublicKey(pair);
        String token = JWT.create()
                .withIssuer("binnur")
                .withClaim("userId", user.getUserId())
                .withClaim("username", user.getName())
                .withClaim("role", user.getRole())
                .withClaim("createdDate", time())
                .sign(Algorithm.ECDSA256(loadPublicKey(), loadPrivateKey()));
        return token;
    }

    private void createPrivateAndPublicKey(KeyPair pair) throws IOException {
        ECPrivateKey privateKey = (ECPrivateKey) pair.getPrivate();
        ECPublicKey publicKey = (ECPublicKey) pair.getPublic();
        Writer out = null;
        String outFile = "C:/Users/feb/Documents/binnur/veterinary/privateKey";
        try {
            if (outFile != null) out = new FileWriter(outFile + ".key");
            else out = new OutputStreamWriter(System.out);
            writeBase64(out, privateKey);
            if (outFile != null) {
                out.close();
                String outFile2 = "C:/Users/feb/Documents/binnur/veterinary/publicKey";
                out = new FileWriter(outFile2 + ".pem");
            }
            writeBase64(out, publicKey);
        } finally {
            if (out != null) out.close();
        }
    }

    public boolean verifyToken(HttpServletRequest request) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        String token = request.getHeader(JwtFilterEnum.AUTHORIZATION.getField()).substring(7);
        try {
            JWTVerifier verifier = JWT.require(Algorithm.ECDSA256(loadPublicKey(), loadPrivateKey()))
                    .withIssuer(TEST)
                    .build();
            verifier.verify(token);
            return true;
        } catch (JWTVerificationException exception) {
            return false;
        }
    }

    public User decodeToken(HttpServletRequest request) throws ServletException {
        try {
            String token = request.getHeader(JwtFilterEnum.AUTHORIZATION.getField()).substring(7);
            DecodedJWT decode = JWT.decode(token);
            String userId = decode.getClaim("userId").as(String.class);
            Optional<User> userOptional = userRepository.findById(userId);
            return userOptional.get();
        } catch (JWTDecodeException exception) {
            throw new ServletException(JwtFilterEnum.INVALID_TOKEN.getField());
        }
    }

    public Map<String, String> whoAmI(String token) throws ServletException {
        try {
            DecodedJWT decode = JWT.decode(token);
            Map<String, String> userMap = new HashMap<>();
            userMap.put("userId", String.valueOf(decode.getClaim("userId").asInt()));
            userMap.put("firstName", decode.getClaim("firstName").asString());
            userMap.put("lastName", decode.getClaim("lastName").asString());
            userMap.put("password", decode.getClaim("password").asString());
            userMap.put("phoneNumber", decode.getClaim("phoneNumber").asString());
            userMap.put("role", decode.getClaim("role").asString());
            return userMap;
        } catch (JWTDecodeException exception) {
            throw new ServletException(INVALID_TOKEN);
        }
    }

    public ECPrivateKey loadPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        Path path = Paths.get(privateKeyPath);
        LOGGER.info("privateKeyPath: " + privateKeyPath);
        byte[] bytes = Files.readAllBytes(path);
        byte[] decode = Base64.getMimeDecoder().decode(bytes);
        PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(decode);
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        ECPrivateKey ecPrivateKey = (ECPrivateKey) keyFactory.generatePrivate(ks);
        return ecPrivateKey;
    }

    public ECPublicKey loadPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        Path path = Paths.get(publicKeyPath);
        LOGGER.info("publicKeyPath: " + publicKeyPath);
        byte[] bytes = Files.readAllBytes(path);
        byte[] decode = Base64.getMimeDecoder().decode(bytes);
        X509EncodedKeySpec ks = new X509EncodedKeySpec(decode);
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        ECPublicKey ecPublicKey = (ECPublicKey) keyFactory.generatePublic(ks);
        return ecPublicKey;
    }

    private static void writeBase64(Writer out, Key key) throws IOException {
        byte[] buf = key.getEncoded();
        out.write(encoder.encodeToString(buf));
        out.write("\n");
    }

    public String getUserToken(String email){
        User byEmail = userRepository.findByEmail(email);
        if(byEmail != null){
            return byEmail.getToken();
        }else{
            return "yok";
        }


    }

}
