package com.binnur.veterinary.util.constant;

public interface TAServiceEnConstant {

    String MSG_FAVORITE_ROUTE_SAVED = "Added favorite route";

    String MSG_FAVORITE_ROUTE_ERROR = "Favorite route not be saved";

    String MSG_FAVORITE_ROUTE_DELETED = "Favorite route removed";

    String MSG_FAVORITE_ROUTE_ALREADY_EXISTS = "Favorite route already exist";


    String MSG_PARK_RESERVATION_DONE = "Parking reservation is made";

    String MSG_PARK_RESERVATION_ERROR = "Parking reservation is not made";

    String MSG_PARK_RESERVATION_REMOVED = "Parking reservation removed";


    String MSG_PAST_ROUTE_SAVED = "Past route is saved";

    String MSG_PAST_ROUTE_ERROR = "Past route not be saved";

    String MSG_PAST_ROUTE_REMOVED = "Past route removed";


    String MSG_SUBSCRIBED_ROUTE_SAVED = "Subscribed to route";

    String MSG_SUBSCRIBED_ROUTE_ERROR = "Could not subscribe to route";

    String MSG_SUBSCRIBED_ROUTE_DELETED = "Route subscription removed";


    String MSG_ACCOUNT_CREATED_SUCCESSFULLY = "Registration was successful";

    String MSG_ACCOUNT_CREATED_ERROR = "Registration failed";


    String MSG_OLD_PASSWORD_WRONG = "Old password is incorrect";

    String MSG_PROFILE_UPDATED = "Profile updated";

    String MSG_PROFILE_ERROR = "Profile could not be updated";


    String MSG_PASSWORD_UPDATED = "Password renewed";

    String MSG_PASSWORD_ERROR = "Password could not be updated";

    String MSG_USER_NOT_FOUND = "User not found";

    String ERROR = "Error";

    String SUCCESS = "Success";


    String MSG_ACCOUNT_DELETED = "Account deleted";


    String PHONE_EXIST = "Phone number exist";

    String VERIFICATION_CODE_ERROR = "Verification code could not be sent";


    String MSG_ACCOUNT_SAVING_WARNING = "Complete the registration process";

    String VERIFICATION_CODE_MATCH_ERROR = "Verification code does not match";


    String MSG_LOGIN_IS_UNSUCCESSFULL = "Login failed. Check the phone number and password";

    String MSG_PHONE_NUMBER_IS_NOT_REGISTERED = "Phone number is not registered in the system";


    String SMS_VALIDATION = "Enter the verification code";


    String SEND_EMAIL_CODE = "Verification code was sent to your email";

    String EXIST_EMAIL = "This email address has already taken";

    String REC_ROUTE_CREATED_SUCCESSFULLY = "Recommended routes are saved";

    String REC_ROUTE_CREATED_ERROR = "Recommended routes could not be saved";

    String MSG_SUGGESTION_COMPLAINT_SAVED = "Suggestion / Complaint saved";

    String MSG_SUGGESTION_COMPLAINT_DELETED = "Suggestion / Complaint deleted";

    String UA_MESSAGE = "Transportation Assistant Verification Code : ";

    String NOTIF_CREATED = "Notification is created";

    String NOTIF_NOT_CREATED = "Notification is not created";

    String MAX_FAV_ROUTE_COUNT = "Maximum 5 routes can be saved";

    String BAD_REQUEST = "Bad Request";

    String NOT_EXIST = "Not Exist";
}