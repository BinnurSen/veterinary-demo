package com.binnur.veterinary.util.constant;

public interface TAServiceTrConstant {

    String MSG_FAVORITE_ROUTE_SAVED = "Favori rotanız eklendi";

    String MSG_FAVORITE_ROUTE_ERROR = "Favori rotanız kayıt edilemedi";

    String MSG_FAVORITE_ROUTE_DELETED = "Favori rotanız silindi";

    String MSG_FAVORITE_ROUTE_ALREADY_EXISTS = "Favori rotanız daha önceden eklenmiş";


    String MSG_PARK_RESERVATION_DONE = "Park rezervasyonu yapıldı";

    String MSG_PARK_RESERVATION_ERROR = "Park rezervasyonu yapılamadı";

    String MSG_PARK_RESERVATION_REMOVED = "Park rezervasyonu kaldırıldı";


    String MSG_PAST_ROUTE_SAVED = "Geçmiş rota kayıt edildi";

    String MSG_PAST_ROUTE_ERROR = "Geçmiş rota kayıt edilemedi";

    String MSG_PAST_ROUTE_REMOVED = "Geçmiş yolculuk kaldırıldı";


    String MSG_SUBSCRIBED_ROUTE_SAVED = "Rotaya abone olundu";

    String MSG_SUBSCRIBED_ROUTE_ERROR = "Rotaya abone olunamadı";

    String MSG_SUBSCRIBED_ROUTE_DELETED = "Rotaya abonelik kaldırıldı";


    String MSG_ACCOUNT_CREATED_SUCCESSFULLY = " Kayıt işleminiz başarıyla gerçekleştirildi";

    String MSG_ACCOUNT_CREATED_ERROR = " Kayıt işleminiz yapılamadı";


    String MSG_OLD_PASSWORD_WRONG = "Eski parola yanlış";

    String MSG_PROFILE_UPDATED = "Profiliniz güncellendi";

    String MSG_PROFILE_ERROR = "Profiliniz güncellenemedi";


    String MSG_PASSWORD_UPDATED = "Şifreniz yenilenmiştir";

    String MSG_PASSWORD_ERROR = "Şifreniz güncellenemedi";

    String MSG_USER_NOT_FOUND = "Kullanıcı bulunamadı";


    String MSG_ACCOUNT_DELETED = "Hesap silme işlemi tamamlandı";


    String PHONE_EXIST = "Kayıtlı telefon numarası";

    String VERIFICATION_CODE_ERROR = "Doğrulama kodu gönderilemedi";


    String MSG_ACCOUNT_SAVING_WARNING = "Kayıt işleminizi tamamlayınız";

    String VERIFICATION_CODE_MATCH_ERROR = "Doğrulama kodu uyuşmuyor";


    String MSG_LOGIN_IS_UNSUCCESSFULL = "Giriş başarısız. Telefon numarası ve şifreyi kontrol ediniz";


    String SMS_VALIDATION = "Doğrulama kodunu giriniz";


    String SEND_EMAIL_CODE = "Mailinize doğrulama kodu gönderildi";


    String MSG_PHONE_NUMBER_IS_NOT_REGISTERED = "Telefon numarası sisteme kayıtlı değil";

    String MSG_MAIL_SAVED = "Mailiniz sisteme başarıyla kaydedilmiştir.";

    String MSG_SUBJECT = "Ulaşım Asistanı";

    String MSG_VERIFICATION_CODE = "Ulaşım Asistanı Doğrulama Kodunuz: ";

    String MSG_INVALID_TOKEN = "Geçersiz sorgu. Token eksik";

    String MSG_ACCOUNT_CREATED = " Kaydiniz olusturuldu ";

    String IMEGE_SAVED = "Profil resmi kaydedildi";

    String IMEGE_NOT_SAVED = "Profil resmi kaydedilemedi";

    String IMEGE_DELETED = "Profil resmi silindi";

    String SUCCESS = "Başarılı";

    String ERROR = "Hata";

    String EXIST_EMAIL = "Mail adresi kullanılmaktadır";

    String REC_ROUTE_CREATED_SUCCESSFULLY = "Önerilen hatlar kaydedildi";

    String REC_ROUTE_CREATED_ERROR = "Önerilen hatlar kaydedilemedi";

    String MSG_SUGGESTION_COMPLAINT_SAVED = "Öneri / Şikayet kayıt edildi";

    String MSG_SUGGESTION_COMPLAINT_DELETED = "Öneri / Şikayet silindi";


    String UA_MESSAGE = "Ulaşım Asistanı Doğrulama Kodunuz : ";

    String FAV_NOTIF_WARNING_TITLE = "Favori Rota Uyarı Bildirimi";

    String FAV_NOTIF_TITLE = "Favori Rota Bildirimi";

    String FAV_NOTIF_CONTENT = "Favori rotanızda uyarı var";

    String NOTIF_CREATED = "Bildirim oluşturuldu";

    String NOTIF_NOT_CREATED = "Bildirim oluşturulmadı";

    String ROUTE_NOTIF = "Rota Bildirimi";

    String CLOSE_TO_STOP_NOTIF = "Bitiş durağına yaklaşılıyor";

    String FAV_ROUTE_STARTS_YET = "Favori rota başlangıcı yaklaşıyor";

    String MAX_FAV_ROUTE_COUNT = "En fazla 5 rota kaydedilebilir";

    String BAD_REQUEST = "Bad Request";

    String NOT_EXIST = "Not Exist";

}