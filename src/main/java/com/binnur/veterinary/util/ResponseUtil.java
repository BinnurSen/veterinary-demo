package com.binnur.veterinary.util;

public class ResponseUtil {

    private static ResponseUtil instance = null;

    public static ResponseUtil getInstance() {
        if (instance == null) {
            instance = new ResponseUtil();
        }
        return instance;
    }

    public GenericResponse createGenericResponse(boolean status, String trMessage, String enMessage){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setStatus(status);
        genericResponse.setTrMessage(trMessage);
        genericResponse.setEnMessage(enMessage);
        return genericResponse;
    }

}
