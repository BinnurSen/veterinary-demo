package com.binnur.veterinary.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class ObjectMapperUtil {

    private static ObjectMapperUtil instance = null;

    private static ObjectMapper objectMapperInstance = null;

    private static String CAN_NOT_CLONE = "Can not clone";

    public static ObjectMapperUtil getInstance() {
        if (instance == null) {
            instance = new ObjectMapperUtil();
        }
        return instance;
    }

    private static ObjectMapper getObjectMapperInstance() {
        if (objectMapperInstance == null) {
            objectMapperInstance = new ObjectMapper();
        }
        return objectMapperInstance;
    }

    public String convertObjectToJson(Object object) throws JsonProcessingException {
        return getObjectMapperInstance().writeValueAsString(object);
    }

    public String convertObjectListToJson(List<?> objectList) throws JsonProcessingException {
        return getObjectMapperInstance().writeValueAsString(objectList);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException(CAN_NOT_CLONE);
    }

}
