package com.binnur.veterinary.util;


import lombok.Data;

import java.io.Serializable;

@Data
public class GenericResponse implements Serializable {

    private boolean status;

    private String trMessage;

    private String enMessage;


}
