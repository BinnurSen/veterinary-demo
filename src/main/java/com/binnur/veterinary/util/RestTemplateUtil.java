package com.binnur.veterinary.util;

import com.binnur.veterinary.util.enums.JwtFilterEnum;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

public class RestTemplateUtil {

    private static RestTemplateUtil instance = null;

    private static final String CAN_NOT_CLONE = "Can not clone";

    public static RestTemplateUtil getInstance() {
        if (instance == null) {
            instance = new RestTemplateUtil();
        }
        return instance;
    }

    public Object createPostRequest(String url, String requestBody, Class<?> responseClassType, String token) {
        RestTemplate restTemplate = createRestTemplate();
        HttpHeaders headers = createHttpHeaders();
        if (token != null) {
            headers.set(JwtFilterEnum.AUTHORIZATION.getField(), JwtFilterEnum.BEARER.getField() + token);
        }
        HttpEntity<String> request = new HttpEntity<>(requestBody, headers);
        ResponseEntity<?> responseEntity = restTemplate
                .exchange(url, HttpMethod.POST, request, responseClassType);
        return responseEntity.getBody();
    }

    public Object createGetRequest(String url, Class<?> responseClassType, String token) {
        RestTemplate restTemplate = createRestTemplate();
        HttpHeaders headers = createHttpHeaders();
        if (token != null) {
            headers.set(JwtFilterEnum.AUTHORIZATION.getField(), JwtFilterEnum.BEARER.getField() + token);
        }
        //headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<?> responseEntity = restTemplate
                .exchange(url, HttpMethod.GET, request, responseClassType);
        return responseEntity.getBody();
    }


    public Object createGetRequestWithBasicAuthorization(String url, String username, String password, Class<?> responseClassType) {
        RestTemplate restTemplate = createRestTemplate();
        restTemplate.getInterceptors().add(
                new BasicAuthorizationInterceptor(username, password));
        ResponseEntity<?> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET, null, responseClassType);
        return responseEntity.getBody();
    }

    private HttpHeaders createHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    private RestTemplate createRestTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        return restTemplate;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException(CAN_NOT_CLONE);
    }

}
