<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <title>Veterinary Demo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Veterinary</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/home">All Users</a></li>
            <li><a href="newUser">New User</a></li>
            <li><a href="allAnimals">All animals</a></li>
            <li><a href="newAnimal">New Animal</a></li>
        </ul>
    </div>
</nav>

<div class="container">


    <c:choose>
        <c:when test="${mode == 'USER_VIEW'}">
            <table class="table table-striped">
                <form action="searchUser" method="GET">
                    <div class="form-inline">
                        <label for="search">search:</label>
                        <input type="text" name="search"  class="form-control" id="search">
                        <button type="submit">Ara</button>
                    </div>
                    <div>

                    </div>
                </form>

                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="userData" items="${userData}">
                    <tr>
                        <td>${userData.userId}</td>
                        <td>${userData.name}</td>
                        <td>${userData.email}</td>
                        <td>${userData.phone}</td>
                        <td>${userData.address}</td>
                        <td><a href="updateUser?userId=${userData.userId}"><div class="glyphicon glyphicon-pencil"></div></a> </td>
                        <td><a href="deleteUser?userId=${userData.userId}"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:when test="${mode == 'USER_EDIT' || mode == 'USER_NEW'}">
            <form action="save" method="POST">
                <c:if test="${mode == 'USER_EDIT'}">
                    <input type="hidden" name="userId" value="${user.userId}" class="form-control" id="userId">
                </c:if>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" value="${user.name}" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="phone">Phone:</label>
                    <input type="text" name="phone" value="${user.phone}" class="form-control" id="phone">
                </div>
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" name="email" value="${user.email}" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" name="password" value="${user.password}" class="form-control" id="pwd">
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" name="address" value="${user.address}" class="form-control" id="address">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </c:when>
        <c:when test="${mode == 'ANIMAL_VIEW'}">
            <table class="table table-striped">
                <form action="searchAnimal" method="GET">
                    <div class="form-inline">
                        <label for="searchAnimal">search animal:</label>
                        <input type="text" name="searchAnimal"  class="form-control" id="searchAnimal">
                        <button type="submit">Ara</button>
                    </div>
                    <div>

                    </div>
                </form>
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Genus</th>
                    <th>Age</th>
                    <th>Description</th>
                    <th>Username</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="animalData" items="${animalData}">
                    <tr>
                        <td>${animalData.animalId}</td>
                        <td>${animalData.name}</td>
                        <td>${animalData.type}</td>
                        <td>${animalData.genus}</td>
                        <td>${animalData.age}</td>
                        <td>${animalData.description}</td>
                        <td>${animalData.userName}</td>
                        <td><a href="updateAnimal?animalId=${animalData.animalId}"><div class="glyphicon glyphicon-pencil"></div></a> </td>
                        <td><a href="deleteAnimal?animalId=${animalData.animalId}"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:when test="${mode == 'ANIMAL_EDIT' || mode == 'ANIMAL_NEW'}">
            <form action="saveAnimal" method="POST">
                <c:if test="${mode == 'ANIMAL_EDIT'}">
                    <input type="hidden" name="animalId" value="${animal.animalId}" class="form-control" id="animalId">
                </c:if>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" value="${animal.name}" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="type">Type:</label>
                    <input type="text" name="type" value="${animal.type}" class="form-control" id="type">
                </div>
                <div class="form-group">
                    <label for="genus">Genus:</label>
                    <input type="text" name="genus" value="${animal.genus}" class="form-control" id="genus">
                </div>
                <div class="form-group">
                    <label for="age">Age:</label>
                    <input type="text" name="age" value="${animal.age}" class="form-control" id="age">
                </div>
                <div class="form-group">
                    <label for="userName">Username:</label>
                    <input type="text" name="userName" value="${animal.userName}" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" name="description" value="${animal.description}" class="form-control" id="description">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </c:when>
    </c:choose>


</div>

</body>
</html>
