<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <title>Veterinary Demo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Veterinary</a>
        </div>
        <ul class="nav navbar-nav">
        </ul>
    </div>
</nav>
<body>
<div class="container">
    ${loginError}
    <div class="form-group form">
        <form action="login" method="post">
            <div>
                <label>Email</label>
                <input type="text" id="email" name="email" placeholder="Email" class="form-control" />
            </div>
            <div>
                <label>Password</label>
                <input type="password" id="password" name="password" placeholder="Password" class="form-control" />
            </div>
            <button id="loginButton" class="form-control">Login</button>
        </form>

    </div>
</div>

</body>
</html>
